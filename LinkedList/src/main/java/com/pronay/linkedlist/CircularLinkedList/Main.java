package com.pronay.linkedlist.CircularLinkedList;

//package import

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        CircularLinkedList circularLL = new CircularLinkedList();
        while (true) {
            // interface for the menu driven program
            System.out.println("1. Create a new list:");
            System.out.println("2. add a node at the beginning:");
            System.out.println("3. add a node at the end:");
            System.out.println("4. add a node at any position:");
            System.out.println("5. Display");
            System.out.println("6. Delete the  first node: ");
            System.out.println("7. Delete the last node: ");
            System.out.println("8. Delete a node at any position: ");
            System.out.println("9. Reverse the list: ");
            System.out.println("10. Search the list: ");
            System.out.println("0. Exit");
            System.out.println("Enter your choice:\t");
            Scanner input = new Scanner(System.in);
            int choice = input.nextInt();
            switch (choice) {
                case 1 -> {
                    CircularLinkedList newList = circularLL.createList(input);
                    circularLL = newList == null ? circularLL : newList;
                }
                case 2 -> circularLL.insert(input);
                case 3 -> circularLL.append(input);
                case 4 -> {
                    System.out.println("Enter the target Value:\t");
                    int targetValue = input.nextInt();
                    circularLL.insert(input, targetValue);
                }
                case 5 -> circularLL.display();
                case 6 -> circularLL.remove();
                case 7 -> circularLL.pop();
                case 8 -> {
                    System.out.println("Enter the target value: ");
                    int targetValue = input.nextInt();
                    circularLL.remove(targetValue);
                }
                case 9 -> circularLL.reverse();
                case 10 -> {
                    System.out.print("Enter the key to be  searched:\t");
                    int key = input.nextInt();
                    circularLL.search(key);
                }
                case 0 -> {
                    System.out.println("exiting...........");
                    input.close();
                    System.exit(0);
                }
                default -> System.out.println("Invalid input");
            }
        }
    }
}
