package com.pronay.linkedlist.CircularLinkedList;

import java.sql.SQLOutput;
import java.util.InputMismatchException;
import java.util.Scanner;

class CircularLinkedList {
    //instance member declaration
    private CircularLLNode head;

    //getter-setter for head

    public void setHead(CircularLLNode head) {
        this.head = head;
    }

    public CircularLLNode getHead(){
        return this.head;
    }

    //constructor
    CircularLinkedList(){
        this.setHead(null);
    }

    //helper method area
    private CircularLLNode nodeCreator(Scanner input){
        System.out.print("Do you want to enter data for the  node:Press 0 for NO\t");
        int choice = input.nextInt();
        if (choice == 0){
             return new CircularLLNode();
        }else{
            System.out.print("Enter data for the new node:\t");
            int data = input.nextInt();
            return new CircularLLNode(data);
        }
    }

    private boolean checkRemoveRoot(){
        if (this.getHead() == null){
            System.out.println("Empty list");
            return true;
        }else if (this.getHead().getNext() == null ){
            System.out.println("Node with data value " + this.getHead().getData() + " is deleted");
            this.setHead(null);
            System.gc();
            return true;
        }
        return false;
    }
    //helper method area

    // instance method

    /*
    * add a node at the beginning of the list
    * input: Scanner object
    * output: void
    * return:  void
     */
    public void insert(Scanner input){
        CircularLLNode newNode = nodeCreator(input);
        if (this.getHead() == null ){
            newNode.setNext(newNode);
            this.setHead(newNode);
        }else{
            CircularLLNode temp = this.getHead();
            while ( temp.getNext() != this.getHead() ) {
                temp = temp.getNext();
            }
            newNode.setNext(this.getHead());
            this.setHead(newNode);
            temp.setNext(newNode);
        }
    }

    /*
    * add a node at the end of the list
    * input: Scanner object
    * output: void
    * return void
     */
    public void append( Scanner input ){
        CircularLLNode newNode = nodeCreator(input);
        if (this.getHead() == null ){
            newNode.setNext(newNode);
            this.setHead(newNode);
        }else{
            CircularLLNode temp = this.getHead();
            while ( temp.getNext() != this.getHead() ){
                temp = temp.getNext();
            }
            temp.setNext(newNode);
            newNode.setNext(this.getHead());
        }
    }
    /*
    * creates a circular linked list based on the number of nodes entered
    * input: Scanner object
    * output: void
    * return: Circular Linked list
     */
    public CircularLinkedList createList(Scanner input){
        int numOfNodes;
        try{
            System.out.print("Enter the number of nodes: ");
            numOfNodes = input.nextInt();
        }catch(InputMismatchException ime){
            numOfNodes = -1;
        }
        System.out.println("Creating list with " + numOfNodes + " nodes");
        if ( numOfNodes < 0 ){
            System.out.println("Invalid Input");
            return null;
        }else{
            CircularLinkedList newList = new CircularLinkedList();
            newList.insert(input);
            numOfNodes -= 1;
            while ( numOfNodes > 0){
                newList.append(input);
                numOfNodes -= 1;
            }
            return newList;
        }
    }

    /*
    * display the content of the list
    * input: void
    * output: data of nodes of the list
    * return: void
     */
    public void display(){
        if (this.getHead() == null ){
            System.out.println("Empty list");
        }else{
            CircularLLNode curr = this.getHead();
            do{
                System.out.print(curr.getData() + "\t");
                curr = curr.getNext();
            }while (curr != this.getHead());
            System.out.println();
        }
    }

    /*
    * add a node after a node with data values given as parameter targetValue
    * input: Scanner Object, integer target value
    * output: void
    * return: void
     */
    public void insert(Scanner input,int targetValue ){
        if (this.getHead() == null ){
            System.out.println("Empty List");
        }else{
            CircularLLNode temp = this.getHead();
            do {
                if (temp.getData() == targetValue){
                    break;
                }
                temp = temp.getNext();
            }while(temp != this.getHead());
            if (temp == null ){
                System.out.println("Node with value " + targetValue + " not found");
            }else{
                CircularLLNode newNode = nodeCreator(input);
                newNode.setNext(temp.getNext());
                temp.setNext(newNode);
            }
        }
    }

    /*
    * delete the root node
    * input:void
    * output: void
    * return: void
     */
    public void remove(){
        if (! this.checkRemoveRoot()){
            CircularLLNode endNode = this.getHead();
            CircularLLNode rootNode = this.getHead();
            this.setHead(rootNode.getNext());
            //loop till the end of the linked list
            while(endNode.getNext() != rootNode){
                endNode = endNode.getNext();
            }
            //set the next pointer of the last node to the new head node
            endNode.setNext(this.getHead());
            rootNode.setNext(null);
            System.out.println("Deleted the root node with data " + rootNode.getData());
            System.gc();
        }
    }

    /*
    * Delete the tail node
    * input: void
    * output: void
    * return: void
     */
    public void pop(){
        if (!this.checkRemoveRoot()){
            CircularLLNode endNode = this.getHead().getNext();
            while(endNode.getNext().getNext() != this.getHead()){
                endNode = endNode.getNext();
            }
            CircularLLNode targetNode = endNode.getNext();
            endNode.setNext(this.getHead());
            System.out.println("Node with data value " + targetNode.getData() + " is deleted");
            targetNode.setNext(null);
            System.gc();
        }
    }

    /*
    * Delete a node with data value as given target value
    * input: integer target value
    * output: void
    * return: void
     */
    public void remove(int targetValue){
        if (this.getHead() == null ) {
            System.out.println("Empty list");
        }
        else{
            CircularLLNode curr = this.getHead(),prev = new CircularLLNode();
            do{
                if(curr.getData() == targetValue){
                    break;
                }
                else{
                    prev = curr;
                    curr = curr.getNext();
                }
            }while( curr != this.getHead());
            if (this.getHead() == curr ){
                //only one node in the list
                if (curr.getNext() == curr){
                    System.out.println("deleting only node with data value:" + targetValue);
                    this.setHead(null);
                }else{
                    prev = curr.getNext();
                    while(prev.getNext() != this.getHead()){
                        prev = prev.getNext();
                    }
                    System.out.println("deleting root node with data value:" + targetValue);
                    this.setHead(curr.getNext());
                    prev.setNext(this.getHead());
                    curr.setNext(null);
                    System.gc();
                }
            }else{
                while(prev.getNext() != curr){
                    prev = prev.getNext();
                }
                System.out.println("deleting node with data value:" + targetValue);
                prev.setNext(curr.getNext());
                curr.setNext(null);
                System.gc();
            }
        }
    }


    /*
    * reverse the circular linked list
    * input: void
    * output: void
    * return: void
     */
    public void reverse(){
        if (this.getHead() == null){
            System.out.println("Empty list");
        }else{
            CircularLLNode curr = this.getHead();
            CircularLLNode prev = null;
            do{
                CircularLLNode next = curr.getNext();
                curr.setNext(prev);
                prev = curr;
                curr = next;
            }while( curr != this.getHead());
            curr.setNext(prev);
            this.setHead(prev);
            System.out.println("List after reversing: ");
            this.display();
        }
    }

    /*
    * search for an element in the list
    * input: integer key value
    * output: print whether the element is present or not
    * return: void
     */
    public void search(int key) {
        if ( this.getHead() == null){
            System.out.println("Empty list");
        }else{
            CircularLLNode curr = this.getHead();
            do {
                if (curr.getData() == key){
                    System.out.println("Node with data value " + key + " exist");
                    return;
                }
                curr = curr.getNext();
            }while(curr != this.getHead());
        }
    }
}
