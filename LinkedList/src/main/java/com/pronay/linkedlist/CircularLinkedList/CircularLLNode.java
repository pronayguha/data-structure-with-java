package com.pronay.linkedlist.CircularLinkedList;

/*
* implementation of singly circular linked list node
 */
class CircularLLNode {
    //instance member declaration
    private int data;
    private CircularLLNode next;

    //getter-setter for data

    public void setData(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }

    // getter-setter for next pointer

    public void setNext(CircularLLNode next) {
        this.next = next;
    }

    public CircularLLNode getNext() {
        return next;
    }

    //constructor
    CircularLLNode(){
//        this.setData(0);
//        this.setNext(null);
        this.data = 0;
        this.next = null;
        System.out.println("Node created with  value: " + this.getData());
    }

    CircularLLNode(int data){
        this.data = data;
        this.next = null;
        System.out.println("Node created with  value: " + this.getData());
    }
}
