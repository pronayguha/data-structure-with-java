package com.pronay.linkedlist.DoubleLinkedList;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    static DoubleLL doubleLL = new DoubleLL();

    public static void main(String[] args) {
        int choice;
        Scanner input =  new Scanner(System.in);
        while (true) {
            // interface for the menu driven program
            System.out.println("1. Create a new list:");
            System.out.println("2. add a node at the beginning:");
            System.out.println("3. add a node at the end:");
            System.out.println("4. add a node at any position:");
            System.out.println("5. Display");
            System.out.println("6. Delete the first node: ");
             System.out.println("7. Delete the last node: ");
             System.out.println("8. Delete a node at any position: ");
             System.out.println("9. Reverse the list: ");
            System.out.println("0. Exit");
            try{
                System.out.println("Enter your choice:\t");
                choice = input.nextInt();
            }catch(InputMismatchException ime){
                choice = 12345;
            }
            switch (choice) {
                case 1 -> {
                    DoubleLL newList = doubleLL.createList(input);
                    doubleLL = newList == null ? doubleLL : newList;
                }

                case 2 -> doubleLL.insert(input);
                case 3 -> doubleLL.append(input);
                case 4 -> {
                    System.out.println("Enter the target Value:\t");
                    int targetValue = input.nextInt();
                    doubleLL.insert(input,targetValue);
                }
                case 5 -> doubleLL.display();
                case 6 -> doubleLL.remove();
                case 7 -> doubleLL.delete();
                case 8 -> {
                    System.out.println("Enter the target value: ");
                    int targetValue = input.nextInt();
                    doubleLL.remove(targetValue);
                }
                case 9 -> doubleLL.reverse();
                case 0 -> {
                    System.out.println("exiting...........");
                    input.close();
                    System.exit(0);
                }
                default -> System.out.println("Invalid input");
            }
        }
    }
}
