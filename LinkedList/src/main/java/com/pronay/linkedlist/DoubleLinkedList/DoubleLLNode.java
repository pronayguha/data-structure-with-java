package com.pronay.linkedlist.DoubleLinkedList;

class DoubleLLNode {
    //instance member declaration
    private DoubleLLNode next,prev;
    private int data;

    // constructor
    DoubleLLNode(){
        this.prev = this.next = null;
        this.data = 0;
        System.out.println("Created a node with data value: 0");
    }

    //parameterized constructor
    DoubleLLNode(int data){
        this.prev = this.next = null;
        this.data = data;
        System.out.println("Created a node with data value: " + this.data);
    }

    //getter setter for instance member prev
    public DoubleLLNode getPrev(){ return this.prev; }

    public void setPrev( DoubleLLNode updatedPrev ){ this.prev = updatedPrev; }

    //getter setter for instance member next
    public DoubleLLNode getNext() { return this.next; }

    public void setNext( DoubleLLNode updatedNext ){ this.next = updatedNext; }

    //getter setter for instance member data
    public int getData(){ return this.data; }

    public void setData( int data ){ this.data = data; }

}
