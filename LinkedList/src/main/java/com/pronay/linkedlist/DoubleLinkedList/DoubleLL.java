package com.pronay.linkedlist.DoubleLinkedList;

import java.util.Scanner;

class DoubleLL {
    // instance member declaration
    private DoubleLLNode head;

    // getter setter method for instance member head
    public DoubleLLNode getHead() {
        return this.head;
    }

    public void setHead(DoubleLLNode updatedHead) {
        this.head = updatedHead;
    }

    // Constructor
    DoubleLL() {
        this.head = null;
    }

    // area for helper methods

    /*
     * desc: create a new node based on whether user want to create a empty node or
     * a node with data value input: Scanner object output:none return: nw node or
     * an object of DoubleLLNode class
     */
    private DoubleLLNode nodeCreator(Scanner input) {
        System.out.print("Do you want to enter data for new node:Press 0 for NO:\t");
        int choice = input.nextInt();
        if (choice == 0) {
            return new DoubleLLNode();
        }
        System.out.print("Enter data for new node:\t");
        int data = input.nextInt();
        return new DoubleLLNode(data);
    }

    // end of helper methods

    // instance methods

    /*
     * desc: add a node at the beginning of the list input: Scanner object
     * output:void return void
     */
    public void insert(Scanner input) {
        if (this.getHead() == null) {
            this.setHead(nodeCreator(input));
        } else {
            DoubleLLNode prevHead = this.getHead();
            DoubleLLNode newHead = nodeCreator(input);
            newHead.setNext(prevHead);
            prevHead.setPrev(newHead);
            this.setHead(newHead);
        }
    }

    /*
     * desc: insert a node at the end of the list input:Scanner object output: void
     * return: void
     */
    public void append(Scanner input) {
        if (this.getHead() == null) {
            System.out.println("Empty List");
            this.setHead(nodeCreator(input));
        } else {
            DoubleLLNode temp = this.getHead();
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            DoubleLLNode newNode = nodeCreator(input);
            temp.setNext(newNode);
            newNode.setPrev(temp);
        }
    }

    /*
     * desc: create a list with given number of nodes input: Scanner Object output:
     * newly created double linked list return: newly created double linked list
     */
    public DoubleLL createList(Scanner input) {
        System.out.println("Enter number of nodes in the list:\t");
        int numOfNodes = input.nextInt();
        if (numOfNodes < 0) {
            System.out.println("invalid input!");
            return null;
        } else if (numOfNodes == 0) {
            return new DoubleLL();
        } else {
            DoubleLL newList = new DoubleLL();
            newList.insert(input);
            while (--numOfNodes > 0) {
                newList.append(input);
            }
            return newList;
        }
    }

    /*
     * desc: add a node after a node with a given target value as parameter input:
     * Scanner object, integer target value output: void return void
     */
    public void insert(Scanner input, int targetValue) {
        if (this.getHead() == null) {
            System.out.println("Empty List");
        } else {
            DoubleLLNode temp = this.getHead();
            while (temp != null && temp.getData() != targetValue) {
                temp = temp.getNext();
            }
            if (temp == null) {
                System.out.println("Node with data " + targetValue + " does not exist");
            } else {
                DoubleLLNode newNode = nodeCreator(input);
                temp.setNext(newNode);
                newNode.setPrev(temp);
            }
        }
    }

    /*
     * desc: display the data value of each node in the list input: void output:
     * data values of each node return: void
     */
    public void display() {
        if (this.getHead() == null) {
            System.out.println("Empty list");
        } else {
            DoubleLLNode temp = this.getHead();
            System.out.println("The  list:------->");
            while (temp != null) {
                System.out.print(temp.getData() + "\t ");
                temp = temp.getNext();
            }
            System.out.println();
        }
    }

    /*
     * desc: delete the head node input: void output: display the updated list
     * return: void
     */
    public void remove() {
        if (this.getHead() == null) {
            System.out.println("Empty List");
        } else if (this.getHead().getNext() == null) {
            DoubleLLNode target = this.getHead();
            System.out.println("Deleting the node with data value " + target.getData());
            this.setHead(null);
            System.gc();
            this.display();
        } else {
            DoubleLLNode target = this.getHead();
            System.out.println("Deleting the node with data value " + target.getData());
            this.setHead(target.getNext());
            this.getHead().setPrev(null);
            target.setNext(null);
            System.gc();
            this.display();
        }
    }

    /*
     * desc: delete the last node of a list input: void output:display the updated
     * list return: void
     */
    public void delete() {
        if (this.getHead() == null) {
            System.out.println("Empty List");
        } else if (this.getHead().getNext() == null) {
            DoubleLLNode target = this.getHead();
            System.out.println("Deleting the node with data value " + target.getData());
            this.setHead(null);
            System.gc();
            this.display();
        } else {
            DoubleLLNode temp = this.getHead();
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            System.out.println("Deleting the node  with the data value: " + temp.getData());
            temp.getPrev().setNext(null);
            System.gc();
            this.display();
        }
    }

    /*
     * desc: delete a node with data value matching with the target value given as
     * parameter input: integer target value output:display the updated list return:
     * void
     */
    public void remove(int targetValue) {
        if (this.getHead() == null) {
            System.out.println("Empty list");
        } else {
            DoubleLLNode temp = this.getHead();
            while (temp != null & temp.getData() != targetValue) {
                temp = temp.getNext();
            }
            if (temp == null) {
                System.out.println("Node with value " + targetValue + " does not exist");
            } else {
                temp.getPrev().setNext(temp.getNext());
                temp.setNext(null);
                temp.setPrev(null);
                System.gc();
                this.display();
            }
        }
    }

    /*
     * desc: reverse a list input: void output: reverse the list and display the
     * reversed list return: void
     */
    public void reverse() {
        if (this.getHead() == null) {
            System.out.println("Empty list");
        } else if (this.getHead().getNext() == null) {
            this.display();
        } else {
            DoubleLLNode prevNode = null;
            DoubleLLNode currNode = this.getHead();
            while (currNode != null) {
                DoubleLLNode nextNode = currNode.getNext();
                currNode.setPrev(nextNode);
                currNode.setNext(prevNode);
                prevNode = currNode;
                currNode = nextNode;
            }
            this.setHead(prevNode);// update the head pointer of the list
        }
        this.display();
    }
}
