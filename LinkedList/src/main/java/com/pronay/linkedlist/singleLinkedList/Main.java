package com.pronay.linkedlist.singleLinkedList;

//package import
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        SingleLL singleLL = new SingleLL();
        while (true) {
            // interface for the menu driven program
            System.out.println("1. Create a new list:");
            System.out.println("2. add a node at the beginning:");
            System.out.println("3. add a node at the end:");
            System.out.println("4. add a node at any position:");
            System.out.println("5. Display");
            System.out.println("6. Delete the  first node: ");
            System.out.println("7. Delete the last node: ");
            System.out.println("8. Delete a node at any position: ");
            System.out.println("9. Reverse the list: ");
            System.out.println("10. Sort the list: ");
            System.out.println("0. Exit");
            System.out.println("Enter your choice:\t");
            Scanner input = new Scanner(System.in);
            int choice = input.nextInt();
            switch (choice) {
                case 1 -> {
                    SingleLL newList = singleLL.createList(input);
                    singleLL = newList == null ? singleLL : newList;
                }

                case 2 -> singleLL.insert(input);
                case 3 -> singleLL.append(input);
                case 4 -> {
                    System.out.println("Enter the target Value:\t");
                    int targetValue = input.nextInt();
                    singleLL.insert(targetValue, input);
                }
                case 5 -> singleLL.display();
                case 6 -> singleLL.delete();
                case 7 -> singleLL.pop();
                case 8 -> {
                    System.out.println("Enter the target value: ");
                    int targetValue = input.nextInt();
                    singleLL.remove(targetValue);
                }
                case 9 -> singleLL.reverse();
                case 0 -> {
                    System.out.println("exiting...........");
                    input.close();
                    System.exit(0);
                }
                case 10 -> {
                    System.out.println("Sorting the list");
                    System.out.println("Here is the sorted list: ");
                    SingleLLNode endNode = singleLL.getHead();
                    while( endNode.getNext() != null ){
                        endNode = endNode.getNext();
                    }
                    singleLL = singleLL.sort(singleLL.getHead(), endNode);
                    singleLL.display();
                }
            }
        }
    }
}
