package com.pronay.linkedlist.singleLinkedList;

import java.util.InputMismatchException;
import java.util.Scanner;
import com.pronay.linkedlist.singleLinkedList.SingleLLNode;

class SingleLL {
    // instance member declaration
    private SingleLLNode head;

    // getter and setter for head

    public SingleLLNode getHead() {
        return this.head;
    }

    public void setHead(SingleLLNode head) {
        this.head = head;
    }

    /*
     * decides whether a data for a node will be supplied or node or not and create
     * a node 
     * input: Scanner object 
     * output: The newly created node
     */

    private SingleLLNode nodeCreator(Scanner input) {
        System.out.println("Do you want to enter data for the node? Press 1 for YES 0 for NO:\t");
        int choice = input.nextInt();
        if (choice == 1) {
            System.out.println("Enter data for the node:\t");
            int data = input.nextInt();
            return new SingleLLNode(data);
        } else {
            return new SingleLLNode();
        }
    }

    /*
     * add a node at the beginning input: Scanner object output: null
     */
    public void insert(Scanner input) {
        if (this.getHead() == null) {
            this.setHead(nodeCreator(input));
        } else {
            SingleLLNode newNode = nodeCreator(input);
            newNode.setNext(this.getHead());
            this.setHead(newNode);
        }
    }

    /*
     * add a node at the end of the list input: Scanner object output: null
     */
    public void append(Scanner input) {
        if (this.getHead() == null) {
            this.setHead(nodeCreator(input));
        } else {
            SingleLLNode temp = this.getHead();
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            temp.setNext(nodeCreator(input));
        }
    }
    /*
     * add a node at the end of the list input: data value output: null
     */
    public void append(int data) {
        if (this.getHead() == null) {
            this.setHead(new SingleLLNode(data));
        } else {
            SingleLLNode temp = this.getHead();
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            temp.setNext(new SingleLLNode(data));
        }
    }

    /*
     * create a list input : Scanner object output : singleLinked list
     */
    public SingleLL createList(Scanner input) {
        int numOfNodes;
        try {
            System.out.println("Enter the number of nodes:\t");
            numOfNodes = input.nextInt();
        } catch (InputMismatchException ime) {
            numOfNodes = 0;
        }
        if (numOfNodes < 0) {
            System.out.println("Invalid input!");
            return null;
        } else if (numOfNodes == 0) {
            return new SingleLL();
        } else if (numOfNodes == 1) {
            SingleLL newList = new SingleLL();
            newList.setHead(newList.nodeCreator(input));
            return newList;
        } else {
            SingleLL newList = new SingleLL();
            newList.setHead(newList.nodeCreator(input));
            while (--numOfNodes > 0) {
                newList.append(input);
            }
            return newList;
        }
    }

    /*
     * add a node after the node with given target value input: value of the target
     * node and Scanner object output: null
     */
    public void insert(int targetValue, Scanner input) {
        if (this.getHead() == null) {
            System.out.println("Empty List");
        } else {
            SingleLLNode temp = this.getHead();
            while (temp != null && temp.getData() != targetValue) {
                temp = temp.getNext();
            }
            if (temp == null) {
                System.out.println("Node with data value " + targetValue + " doesn't exist");
            } else {
                System.out.println("Node with target value " + targetValue + " found");
                SingleLLNode newNode = nodeCreator(input);
                newNode.setNext(temp.getNext());
                temp.setNext(newNode);
            }
        }
    }

    /*
     * display the list input: null output: display the node values starting from
     * the root node return : void
     */

    public void display() {
        if (this.getHead() == null) {
            System.out.println("Empty List");
        } else {
            SingleLLNode curr = this.getHead();
            do {
                System.out.print(curr.getData() + "\t");
                curr = curr.getNext();
            } while (curr != null);
            System.out.println();
        }
    }

    /*
     * desc: delete the root/ first node of the list input:null output: void return
     * : void
     */
    public void delete() {
        if (this.getHead() == null) {
            System.out.println("Empty List");
        } else {
            System.out.println("The node to be deleted is the  node with the value : " + this.getHead().getData());
            SingleLLNode targetNode = this.getHead();
            this.setHead(targetNode.getNext());
            targetNode.setNext(null);
            System.gc();
            this.display();
        }
    }

    /*
     * desc: delete the last node of the list input: null output : void return :
     * void
     */
    public void pop() {
        if (this.getHead() == null) {
            System.out.println("Empty List");
        } else {
            SingleLLNode temp = this.getHead();
            while (temp.getNext().getNext() != null) {
                temp = temp.getNext();
            }
            System.out.println("The node to be deleted is the  node with the value : " + temp.getNext().getData());
            temp.setNext(null);
            System.gc();
            this.display();
        }
    }

    /*
     * desc: delete the first occurring node with a given value as input input:
     * integer target value output: void return void
     */
    public void remove(int targetValue) {
        if (this.getHead() == null) {
            System.out.println("Empty List");
        } else {
            SingleLLNode temp = this.getHead();
            while (temp != null && temp.getNext().getData() != targetValue) {
                temp = temp.getNext();
            }
            if (temp == null) {
                System.out.println("Node with data value " + targetValue + " doesn't exist");
            } else {
                SingleLLNode targetNode = temp.getNext();
                temp.setNext(targetNode.getNext());
                targetNode.setNext(null);
                System.gc();
                System.out.println("The Updated List:----------");
                this.display();
            }
        }
    }

    /*
     * desc: reverse the list input: void output: the list in reverse order return
     * null
     */
    public void reverse() {
        if (this.getHead() == null) {
            System.out.println("Empty List");
        } else if (this.getHead().getNext() == null) {
            System.out.println("The reversed list");
            this.display();
        } else {
            SingleLLNode curr = this.getHead();
            SingleLLNode prev = null;
            while (curr != null) {
                SingleLLNode next = curr.getNext();
                curr.setNext(prev);
                prev = curr;
                curr = next;
            }
            System.out.println("The reversed list: ");
            this.display();
        }
    }
    
    /*
    * input : head,varargs end
    * output: middle element of the list
    * return: SingleLLNode
    */
    public SingleLLNode findMid(SingleLLNode start, SingleLLNode end){
        SingleLLNode slow = start;
        SingleLLNode fast = start;
        while(fast != end && fast.getNext() != end ) {
            fast = fast.getNext().getNext();
            slow = slow.getNext();
        }
        return slow;
    }
    
    /*
    * input: head node of the two sorted linked list
    * output: merged list
    * return: head node of the merged list
    */
    public SingleLL mergeSorted(SingleLL firstList, SingleLL secondList){
        SingleLL mergedList  = new SingleLL();
        SingleLLNode firstPtr = firstList.getHead();
        SingleLLNode secondPtr = secondList.getHead();
        while( firstPtr != null && secondPtr != null){
            if(firstPtr.getData() >= secondPtr.getData() ){
                mergedList.append(secondPtr.getData());
                secondPtr = secondPtr.getNext();
            }else{
                mergedList.append(firstPtr.getData());
                firstPtr= firstPtr.getNext();
            }
        }
        
        //first list is empty and element in the second list 
        while( secondPtr != null ){
            mergedList.append(secondPtr.getData());
            secondPtr = secondPtr.getNext();
        }
        

        //first list is empty and element in the second list 
        while( firstPtr != null ){
            mergedList.append(firstPtr.getData());
            firstPtr = firstPtr.getNext();
        }
        
        
        return mergedList;
    }
    
    /*
    * desc: sort the linked list using merge sort technique
    * input: void
    * output: sorted list
    * return: void
    */
    
    public SingleLL sort(SingleLLNode start, SingleLLNode end){
            if (start == end ){
                SingleLL br = new SingleLL();
                br.append(start.getData());
                return br;
            }
            SingleLLNode mid = findMid(start,end);
            SingleLL fsh = sort(start, mid);
            SingleLL ssh = sort(mid.getNext(), end);
            SingleLL output = mergeSorted(fsh,ssh);
            System.out.println("List to be returned: ");
            SingleLLNode head_output = output.getHead();
            while(head_output != null ){
                System.out.print(head_output.getData() + "   ");
                head_output = head_output.getNext();
            }
            return output;            
    }
}
