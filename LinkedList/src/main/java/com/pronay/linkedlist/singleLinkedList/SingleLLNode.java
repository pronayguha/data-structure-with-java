package com.pronay.linkedlist.singleLinkedList;

public class SingleLLNode {
    //instance member declaration
    private int data;
    private SingleLLNode next;

    //constructor(default)
    SingleLLNode(){
        System.out.println("creating your node with  dat value 0");
        this.data = 0;
        this.next = null;
    }

    //constructor parameterized(data only)

    SingleLLNode(int data){
        System.out.println("creating your  node with data value " + data);
        this.data = data;
        this.next = null;
    }


    //getter setter method for data

    public int getData() {
        return this.data;
    }

    public void setData(int data) {
        this.data = data;
    }

    //getter and setter for next pointer

    public SingleLLNode getNext() {
        return this.next;
    }

    public void setNext(SingleLLNode next) {
        this.next = next;
    }
}
