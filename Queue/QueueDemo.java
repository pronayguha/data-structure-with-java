import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;


/**
 *
 * @author user
 */
public class QueueDemo {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        Queue<Integer> queue = new LinkedList<>();
        queue.add(10);
        queue.add(20);
        queue.add(30);
        System.out.print("Enter a value to search: ");
        Integer searchVal = sc.nextInt();
        System.out.println("Queue contains element " + searchVal + " or not?"  + queue.contains(searchVal));
        Iterator itr = queue.iterator();
        while(itr.hasNext()){
            System.out.println((Integer)itr.next());
        }
    }
}
