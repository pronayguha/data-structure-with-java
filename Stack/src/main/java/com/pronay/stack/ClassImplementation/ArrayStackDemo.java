package com.pronay.stack.ClassImplementation;


import java.util.InputMismatchException;
import java.util.Scanner;
/**
 *
 * @author user
 */

class Stack {
    private int[] stack;
    private int top;

    Stack() {
        this.stack = new int[5];
        this.top = -1;
    }
    
    Stack(int size){
        this.stack = new int[size];
        this.top = -1;
    }
    
    
    public boolean isFull() {
        return this.top == this.stack.length - 1;
    }
    
    public boolean isEmpty () {
        return this.top == -1 ;
    }
    
    
    public void push(int value){
        if(isFull()){
            System.out.println("Stack Overflow");
        }else{
            int targetIndex = this.top + 1;
            System.out.println(value + " is inserted at position " + targetIndex);
            this.stack[targetIndex] = value;
            this.top += 1;
        }
    }
    
    public void pop(){
        if(isEmpty() ){
            System.out.println("Stack Underflow");
        }else{
            int poppedValue = this.stack[this.top];
            this.top -= 1;
            System.out.println("Popped Value: " + poppedValue);
        }
    }
    
    
    public void peek() {
        if( isEmpty()){
            System.out.println("Empty Stack");
        }else{
            System.out.println("Value at the top: " + this.stack[this.top]);
        }
    }
    
    
}
public class ArrayStackDemo {
    public static void main(String[] args) {
        Stack newStack = new Stack();
        Scanner sc = new Scanner(System.in);
        while(true){
            int choice;
            System.out.println("1. Create A  new Stack ");
            System.out.println("2. Push item into the stack ");
            System.out.println("3. Pop an item from the stack ");
            System.out.println("4. Peek into the stack ");
            System.out.println("0. Exit ");
            System.out.println("Enter your choice:  ");
            try{
                choice = sc.nextInt();
            }catch(InputMismatchException ime){
                System.out.println("Invalid input ");
                choice = 0;
            }
            
            switch(choice){
                case 1 -> {
                    System.out.print("Press 1 to enter size of the stack: ");
                    int dcsn = sc.nextInt();
                    if (dcsn == 1 ){
                        System.out.print("Enter the size of your stack: ");
                        int size ;
                        try{
                            size = sc.nextInt();
                        }catch(InputMismatchException ime){
                            System.out.println("Invalid input");
                            break;
                        }
                        if (size > 0 ){
                            System.out.println("Creating a stack of size " + size);
                            newStack = new Stack(size);
                        }
                    }else{
                        System.out.println("Creating a stack of size 5");
                        newStack  = new Stack();
                    }
                }
                case 2 -> {
                    int data;
                    System.out.print("Enter data:   ");
                    try{
                        data = sc.nextInt();
                    }catch(InputMismatchException ime ){
                        System.out.println("Invalid input " + ime.getMessage());
                        break;
                    }
                    newStack.push(data);
                }
                case 3 -> newStack.pop();
                case 4 -> newStack.peek();
                case 0 -> {
                    System.out.println("Exiting.....");
                    sc.close();
                    System.exit(0);
                }
            }
        }
    }
}
