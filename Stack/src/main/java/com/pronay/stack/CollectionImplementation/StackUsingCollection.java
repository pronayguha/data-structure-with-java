package com.pronay.stack.CollectionImplementation;

import java.util.Stack;
import java.util.EmptyStackException;
import java.util.Scanner;
import java.util.InputMismatchException;


/**
 *
 * @author Pronay
 */
class StackCollection{
    private Stack<Integer> stack;
    
    
    StackCollection() {
        this.stack = new Stack<>();
    }
    
    
    StackCollection(int size) {
        this.stack = new Stack<>();//creaation of a new empty Stack
        this.stack.setSize(size);//setting the capacity of the stack
    }
    
    public void stack_push(Integer data) {
        this.stack.push(data);
    }
    
    
    public void stack_pop(){
        try {
            Integer poppedVal = this.stack.pop();
            if (poppedVal == null ){
                System.out.println("Empty Stack");
            }else{
                System.out.println("Popped Value: " + poppedVal);
            }
        }catch( EmptyStackException ese){
            System.out.println("Empty Stack");
        }
    }
    
    public void stack_peek(){
        try {
            Integer peekedVal = this.stack.peek();
            if (peekedVal != null ){
                System.out.println("Element at the top of the stack: " + this.stack.peek());
            }else{
                System.out.println("Empty Stack");
            }
        }catch( EmptyStackException ese){
            System.out.println("Empty Stack");
        }
    }
    
    /*-----------end of stackCollection class-----------*/
}

public class StackUsingCollection {
    public static void main(String[] args) {
        StackCollection stack = new StackCollection();
        Scanner sc = new Scanner(System.in);
        while(true) {
            System.out.println("1. Create a new Stack ");
            System.out.println("2. Push an item ");
            System.out.println("3. Pop an item ");
            System.out.println("4. Peek ");
            System.out.println("0. Exit");
            System.out.print("Enter your choice: ");
            int choice;
            try {
                choice = sc.nextInt();
            }catch(InputMismatchException ime){
                System.out.println("Invalid Input");
                choice = 0;
            }
            switch(choice){
                case 1 -> {
                    char dcsn;
                    System.out.println("Press Y to create stack of given size ");
                    System.out.print("Else press N: ");
                    try{
                        dcsn = sc.next().charAt(0);
                        if( dcsn == 'Y' || dcsn == 'y' ){
                            int size;
                            System.out.print("Enter the size of the Stack: ");
                            try{
                                size = sc.nextInt();
                                System.out.println("Creating stack of size: " + size);
                                stack = new StackCollection(size);
                            }catch(InputMismatchException ime ){
                                System.out.println("Invalid Input");
                            }catch(Exception e){
                                System.out.println("Exception Occurred " + e.getMessage());
                            }
                        }else{
                            stack = new StackCollection();
                        }
                    }catch(InputMismatchException ime ){
                        System.out.println("Invalid Input");
                    }catch(Exception e){
                        System.out.println("Exception Occurred " + e.getMessage());
                    }
                }
                case 2 ->{
                    int data;
                    System.out.print("Enter the value to be pushed: ");
                    try{
                        data = sc.nextInt();
                        stack.stack_push(Integer.valueOf(data));
                    }catch(InputMismatchException ime ){
                        System.out.println("Invalid Input");
                    }catch(Exception e){
                        System.out.println("Exception Occurred " + e.getMessage());
                        e.printStackTrace();
                    }
                }
                case 3-> stack.stack_pop();
                case 4 -> stack.stack_peek();
                case 0 -> {
                    System.out.println("Exiting ......");
                    sc.close();
                    System.exit(0);
                }
                default -> System.out.println("Invalid input");
            }
        }
    }
}
