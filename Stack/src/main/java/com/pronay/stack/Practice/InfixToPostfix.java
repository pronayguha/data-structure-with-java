package com.pronay.stack.Practice;

import java.util.Stack;
import java.util.Scanner;

/**
 *
 * @author pronay
 */
public class InfixToPostfix {

    public static int checkPrec(char ch) {
        switch (ch) {
            case '(' : {
                return Integer.MIN_VALUE;
            }
            case '+':
            case '-' : {
                return 1;
            }
            case '*' :
            case '/' : {
                return 2;
            }
            case '%' : {
                return 3;
            }
            default : {
                return Integer.MAX_VALUE;
            }
        }
    }

    public static void convertInfixToPostfix(String expr) {
        if (expr.length() < 1) {
            System.out.println("Empty String");
        } else if (expr.length() == 1) {
            if (expr.charAt(0) == ')' || expr.charAt(0) == ')') {
                System.out.println("Invalid Expression");
            } else {
                System.out.println(expr);
            }
        } else {
            Stack<Character> stack = new Stack<>();
            String output = "";
            for (int i = 0; i < expr.length(); i++) {
                char ch = expr.charAt(i);
                if ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122)) {
                    output += ch;
                } else if (ch == '(') {
                    stack.push(ch);
                } else if (ch == ')') {
                    while (!stack.isEmpty() && (char) stack.peek() != '(') {
                        output += (char) stack.pop();
                    }
                    if (stack.isEmpty()) {
                        System.out.println(output);
                        System.exit(0);
                    }
                } else {
                    int precedence = checkPrec(ch);
                    while (!stack.isEmpty() && precedence <= checkPrec((char) stack.peek())) {
                        char poppedItem = (char) stack.pop();
                        output += poppedItem;
                    }
                    stack.push(ch);
                }
            }
            while (!stack.isEmpty()) {
                char poppedItem = (char) stack.pop();
                if (poppedItem != '(') {
                    output += poppedItem;
                }
            }
            System.out.println(output);
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the expression: ");
        String expression = sc.next();
        convertInfixToPostfix(expression);
        sc.close();
    }
}
